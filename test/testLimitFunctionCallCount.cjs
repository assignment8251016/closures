const limitFunctionCallCount = require("../limitFunctionCallCount.cjs")
const tfun = function tfun(a,b,c,d,e){
    return a+b+c+d+e;
}
const func = limitFunctionCallCount(tfun,5)
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))
console.log("returned value", func(1,2,3,4,5))