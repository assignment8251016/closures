const cacheFunction = require("../cacheFunction.cjs")
const func = cacheFunction((a,b)=>{return a+b})
console.log("value cached!",func(1,2),"\n")
console.log("cached value:",func(1,2));
console.log("cached value:",func(1,2),"\n")
console.log(func(4,3));
console.log("value cached!",func(2,1));
console.log()
console.log("cached value:")
console.log(func(2,1));