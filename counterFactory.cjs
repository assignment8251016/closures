function counterFactory(){
    let x = 0;
    return {
        increment(){
            return ++x;
        },
        decrement(){
            return --x;
        }
    };
}

module.exports = counterFactory;