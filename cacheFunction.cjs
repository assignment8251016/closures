function cacheFunction(cb){
    let cache = {result:{}};
    let count = 0;
    if(cb === undefined){
        throw new Error("callback function parameter not satisfied.")
    }
    return function callCallback(){
        let chk = false;

        for(let mainkey in cache){
            chk = false;
            for(let key in cache[mainkey]){
                if(arguments[key] === cache[mainkey][key]){
                    chk=true
                }else{
                    chk=false
                }
            }

            if (chk === true){
                return cache.result[mainkey];
            }
        }

        
        cache[count]  = arguments;
        cache.result[count] = cb(...arguments);
        count+=1
        return cache.result[count-1];
    }
}   

module.exports = cacheFunction;