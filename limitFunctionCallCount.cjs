function limitFunctionCallCount(cb, n){
    let count=0;
    if(cb===undefined || n===undefined){
        throw new Error("incomplete parameters.")
    }

    return function temp(){
        let temp = undefined;
        if (count<n){
            temp =  cb(...arguments);
        }

        count+=1;
        return temp===undefined?null:temp;
    };
}

module.exports = limitFunctionCallCount;